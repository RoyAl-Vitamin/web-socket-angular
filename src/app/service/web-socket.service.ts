import { Injectable } from '@angular/core';
import { Subject } from 'rxjs';
import { Observable } from 'rxjs';
import { Message } from '../model/message';
import { MessageHeader } from '../model/message-header';
import { MessageType } from '../model/message-type';

@Injectable({
  providedIn: 'root'
})
export class WebSocketService {

  private webSocket!: WebSocket;

  private messageObserver = new Subject<Message>();

  private connectCountObserver = new Subject<Number>();

  private readonly onConnectMessage = new MessageHeader(MessageType.onConnect, undefined);

  private readonly onDisconnectMessage = new MessageHeader(MessageType.onDisconnect, undefined);

  constructor() { }

  public connect(): void {
    this.webSocket = new WebSocket("ws://localhost:8080/chat");

    this.webSocket.onopen = (event) => {
      this.webSocket.send(JSON.stringify(this.onConnectMessage));
      console.log("WebSocket is Open!");
    }

    this.webSocket.onmessage = (event) => {
      var message = JSON.parse(event.data);
      switch (message.type) {
        case MessageType.connectCount:
          this.connectCountObserver.next(message.data as Number);
          break;
        case MessageType.message:
          this.messageObserver.next(message.data as Message);
          break;
        default:
          break;
      }
      console.log("WebSocket consume = " + event.data);
    }

    this.webSocket.onclose = (event) => {
      this.messageObserver.complete();
      this.connectCountObserver.complete();
      this.webSocket.send(JSON.stringify(this.onDisconnectMessage));
      console.log("WebSocket is Close!");
    }
  }

  public disconnect(): void {
    this.webSocket.close();
  }
  
  public send(message: Message): void {
    console.log("WebSocket produce = " + message.text);
    if (this.webSocket.readyState === WebSocket.OPEN) {
      this.webSocket.send(JSON.stringify(new MessageHeader(MessageType.message, message)));
    }
  }

  public listen() : Observable<Message> {
    return this.messageObserver;
  }

  public listenConnectCount() : Observable<Number> {
    return this.connectCountObserver;
  }
}
