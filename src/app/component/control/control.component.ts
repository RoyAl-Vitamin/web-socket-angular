import { Component, EventEmitter, Output, ViewChild } from '@angular/core';
import { Message } from 'src/app/model/message';

@Component({
  selector: 'app-control',
  templateUrl: './control.component.html',
  styleUrls: ['./control.component.css']
})
export class ControlComponent {

  constructor() { }

  @Output()
  messageEmitter: EventEmitter<Message> = new EventEmitter<Message>();

  @ViewChild('textItem', { static: true } ) public textField: any;

  public send(author: string, text: string): void {
    let message = new Message();
    message.author = author;
    message.text = text;
    this.messageEmitter.emit(message);
    this.textField.nativeElement.value = '';
  }
}
