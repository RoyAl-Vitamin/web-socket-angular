import { Component, OnInit, ViewChild } from '@angular/core';
import { Message } from '../../model/message';

@Component({
  selector: 'app-pane',
  templateUrl: './pane.component.html',
  styleUrls: ['./pane.component.css']
})
export class PaneComponent implements OnInit {

  @ViewChild('container', { static: true } ) public container: any;
  public row: any;

  constructor() { }

  ngOnInit(): void {
  }

  public createRow(message: Message) {
    console.log("MESSAGE = " + message.text);
    let row = document.createElement("div");
    row.innerHTML = `<span class="author">${message.author}</span><br>` + `<span class="red">${message.text}</span>`;
    this.container.nativeElement.appendChild(row);
  }
}
