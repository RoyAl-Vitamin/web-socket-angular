import { Component, OnDestroy, OnInit, ViewChild } from '@angular/core';
import { Message } from './model/message';
import { HeaderComponent } from './component/header/header.component';
import { PaneComponent } from './component/pane/pane.component';
import { WebSocketService } from './service/web-socket.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit, OnDestroy {

  constructor(public webSocketService: WebSocketService) { }

  ngOnInit(): void {
    this.connect();
  }

  ngOnDestroy(): void {
    this.disconnect();
  }

  @ViewChild(PaneComponent)
  private paneComponent!: PaneComponent;

  @ViewChild(HeaderComponent)
  private headerComponent!: HeaderComponent;

  public connect(): void {
    console.log("it's time to connect!");
    this.webSocketService.connect();
    this.webSocketService.listen().subscribe((message: Message) => this.paneComponent.createRow(message));
    this.webSocketService.listenConnectCount().subscribe((count: Number) => this.headerComponent.updateCount(count));
  }

  public disconnect(): void {
    console.log("it's time to disconnect!");
    this.webSocketService.disconnect();
  }

  public send(message: Message): void {
    console.log("author: " + message.author + "text: " + message.text);
    this.webSocketService.send(message);
    this.paneComponent.createRow(message);
  }
}
