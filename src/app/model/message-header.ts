import { Message } from './message';
import { MessageType } from './message-type';

export class MessageHeader<T> {

    public type: MessageType;

    public data!: T;

    public constructor(type: MessageType, data?: T) {
        this.type = type;
        if (data instanceof Message) {
            this.data = data;
        }
    }
}
