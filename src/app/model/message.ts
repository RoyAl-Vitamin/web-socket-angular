export class Message {

    /**
     * Автор сообщения
     */
    public author!: string;

    /**
     * Текст сообщения
     */
    public text!: string;
}
