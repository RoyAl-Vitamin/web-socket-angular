export enum MessageType {

    onConnect = "ON_CONNECT",
    onDisconnect = "ON_DISCONNECT",
    message = "MESSAGE",
    connectCount = "CONNECT_COUNT",
}
